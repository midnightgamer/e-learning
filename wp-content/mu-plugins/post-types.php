<?php
function learning_post_types(){
/*
register_post_type('campus',array(
	'has_archive' => true,
	'supports' =>array('title','editor','excerpt' ),
	'rewrite' => array('slug' => 'campuses'),
        'public' => true,
        'labels' => array(
            'name' => 'Campuses',
            'add_new_item' => 'Add New Campus',
	     'edit_item' => 'Edit Campus',
		'all_items' => 'All Campus',
		'singular_name' => 'Campus'
        ),
        'menu_icon' => 'dashicons-location-alt'
    )); */
//Hacks

    register_post_type('softwares',array(
	'has_archive' => true,
	'supports' =>array('title','editor','excerpt','author' ),
	'rewrite' => array('slug' => 'softwares'),
        'public' => true,
	'taxonomies'    => array( 'category' ),
        'labels' => array(
            'name' => 'Softwares',
            'add_new_item' => 'Add New Software',
	     'edit_item' => 'Edit Softwares',
		'all_items' => 'All Softwares',
		'singular_name' => 'Software',
        ),
        'menu_icon' => 'dashicons-pressthis'
    )); 

/*
    register_post_type('event',array(
'capability_type' => 'event',
	'map_meta_cap' => true,
	'has_archive' => true,
	'supports' =>array('title','editor','excerpt' ),
	'rewrite' => array('slug' => 'events'),
        'public' => true,
        'labels' => array(
            'name' => 'Events',
            'add_new_item' => 'Add New Event',
	     'edit_item' => 'Edit Event',
		'all_items' => 'All Events',
		'singular_name' => 'Event'
        ),
        'menu_icon' => 'dashicons-calendar'
    )); 

//Professprs

register_post_type('professor',array(
	'show_in_rest'=> true,
	'supports' =>array('title','editor','excerpt','thumbnail' ),
        'public' => true,
        'labels' => array(
            'name' => 'Professor',
            'add_new_item' => 'Add New Professor',
	     'edit_item' => 'Edit Professor',
		'all_items' => 'All Professors',
		'singular_name' => 'Professor'
        ),
        'menu_icon' => 'dashicons-welcome-learn-more'
    ));
*/

//Enroll

     register_post_type('coures',array(
	'capability_type' => 'coures',
	'map_meta_cap' => true,
	'has_archive' => true,
	'supports' =>array('title','editor','excerpt' ),
	'rewrite' => array('slug' => 'coures'),
        'public' => true,
        'labels' => array(
            'name' => 'Coures',
            'add_new_item' => 'Add New Coures',
	     'edit_item' => 'Edit Coures',
		'all_items' => 'All Coures',
		'singular_name' => 'Coures '
        ),
        'menu_icon' => 'dashicons-welcome-add-page'
    )); 

//Like


     register_post_type('like',array(
	'supports' =>array('title' ),
        'public' => false,
	'show_ui' => true,
        'labels' => array(
            'name' => 'Likes',
            'add_new_item' => 'Add New Likes',
	     'edit_item' => 'Edit Likes',
		'all_items' => 'All Likes',
		'singular_name' => 'Like '
        ),
        'menu_icon' => 'dashicons-heart'
    )); 

    register_post_type('blog',array(
	'has_archive' => true,
	'supports' =>array('title','editor','excerpt','author' ),
	'rewrite' => array('slug' => 'blogs'),
        'public' => true,
	'taxonomies'    => array( 'category' ),
        'labels' => array(
            'name' => 'Blog',
            'add_new_item' => 'Add New Blog',
	     'edit_item' => 'Edit Blogs',
		'all_items' => 'All Blogs',
		'singular_name' => 'Blog',
        ),
        'menu_icon' => 'dashicons-admin-customizer'
    )); 


    register_post_type('game',array(
	'has_archive' => true,
	'supports' =>array('title','editor','excerpt','author' ),
	'rewrite' => array('slug' => 'games'),
        'public' => true,
	'taxonomies'    => array( 'category' ),
        'labels' => array(
            'name' => 'Game',
            'add_new_item' => 'Add New Game',
	     'edit_item' => 'Edit Games',
		'all_items' => 'All Games',
		'singular_name' => 'Game',
        ),
        'menu_icon' => 'dashicons-album'
    )); 


}



add_action('init','learning_post_types'); 


 ?>
