<footer class="site-footer">

    <div class="site-footer__inner container container--narrow">

        <div class="group">

            <div class="site-footer__col-one">
                <h1 class="school-logo-text school-logo-text--alt-color"><a
                            href=" <?php echo site_url() ?>"><strong>E-</strong>
                        Learning</a></h1>
                <!--                <p><a class="site-footer__link" href="#">555.555.5555</a></p>-->
            </div>

            <div class="site-footer__col-two-three-group">
                <div class="site-footer__col-two">
                    <h3 class="headline headline--small">Explore</h3>
                    <nav class="nav-list">
                        <ul>
                            <li><a href=" <?php echo site_url('index.php/games') ?>">Games</a></li>
                            <li><a href="<?php echo site_url('index.php/blogs') ?>"">Course</a></li>
                            <li><a href="<?php echo site_url('index.php/coures') ?>"">Enrollable</a></li>
                            <li><a href="<?php echo site_url('index.php/softwares') ?>"">Software</a></li>
                            <!-- <?php
                            wp_nav_menu(array(
                                'theme_location' => 'footerLocationOne'
                            ))
                            ?> -->
                        </ul>
                    </nav>
                </div>

                <!--                <div class="site-footer__col-three">-->
                <!--                    <h3 class="headline headline--small">Learn</h3>-->
                <!--                    <nav class="nav-list">-->
                <!--                        <ul>-->
                <!--                            <!-- --><?php
                //                            wp_nav_menu(array(
                //                                'theme_location' => 'footerLocationTwo'
                //                            ))
                //                            ?><!-- -->
                <!--                            <li><a href="#">Legal</a></li>-->
                <!--                            <li><a href="#">Privacy</a></li>-->
                <!--                            <li><a href="#">Careers</a></li>-->
                <!--                        </ul>-->
                <!--                    </nav>-->
                <!--                </div>-->
            </div>

            <div class="site-footer__col-four">
                <h3 class="headline headline--small">Connect With Us</h3>
                <nav>
                    <ul class="min-list social-icons-list group">
                        <li><a href="#" class="social-color-facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        </li>
                        <li><a href="https://twitter.com/coursespoints?lang=en" class="social-color-twitter"><i class="fa fa-twitter"
                                                                        aria-hidden="true"></i></a></li>
                        <li><a href="https://www.youtube.com/channel/UCCD3KWUy5EBL0R14GhNPWkw?view_as=subscriber" class="social-color-youtube"><i class="fa fa-youtube"
                                                                        aria-hidden="true"></i></a></li>
                        <li><a href="#" class="social-color-instagram"><i class="fa fa-instagram"
                                                                          aria-hidden="true"></i></a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <p class="author">All Rights Reserved &copy;E-Learning. Website is made by <a
                href="https://www.instagram.com/midnight_gamer_/">Pankaj Jaiswal</a></p>
</footer>
<?php wp_footer(); ?>
</body>
</html>