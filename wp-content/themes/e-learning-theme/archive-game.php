<?php
get_header();
pageBanner(array(
    'title' => get_the_archive_title(),
    'subTitle' => get_the_archive_description(),
    'photo' => 'https://marketplace.canva.com/MABY0mFVH4M/1/0/thumbnail_large/canva-bright-colored-tumblr-banner-MABY0mFVH4M.jpg',
))
?>
    <div class="container container--narrow page-section game-container">
        <?php
        while (have_posts()) {
        the_post(); ?>
        <a href="<?php the_permalink() ?>">
            <div class="game-post">
                <div class="game-box">
                    <img src="<?php echo get_field('image_link') ?>" alt="Game Preview">
                    <div class="game-info">
                      <div class="cat_name">
                        <?php echo get_the_category_list(', ') ?>
                      </div>
                        <p class="game-title"><?php the_title(); ?> </p>
                        <p><?php the_author_posts_link() ?></p>

                    </div>
                </div>
            </div>
        </a>
            <?php
            }
            echo paginate_links();
            ?>
    </div>
<?php


get_footer();
?>