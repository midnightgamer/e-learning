<?php get_header() ?>

<div class="page-banner">
    <div class="page-banner__bg-image"
         style="background-image: url(https://images.pexels.com/photos/270348/pexels-photo-270348.jpeg?auto=compress&cs=tinysrgb&h=350);"></div>
    <div class="page-banner__content container t-center c-white">
        <h1 class="headline headline--large"><?php if(is_user_logged_in()){ ?> Hi!<?php } else{ ?> Welcome!<?php } ?></h1>
        <h2 class="headline headline--medium">We think you&rsquo;ll like it here.</h2>
        <h3 class="headline headline--small">Why don&rsquo;t you check out the <strong>major</strong> you&rsquo;re
            interested in?</h3>
        <a href="<?php echo site_url('index.php/blogs'); ?>" class="btn btn--large btn--orange2">Find Your Major</a>
    </div>

</div>
<div class="full-width-split group p-bottom-large">
    <div class="full-width-split__one">
        <div class="full-width-split__inner">
            <h2 class="headline headline--small-plus t-center">Enrollable Courses</h2>
            <?php
            $posts = new WP_Query(array(
                'posts_per_page' => 3,
                'post_type' => 'coures',
            ));

            while ($posts->have_posts()) {
                $posts->the_post(); ?>
                <div class="event-summary">
                    <a class="event-summary__date event-summary__date--beige t-center" href="<?php the_permalink(); ?>">
                        <span class="event-summary__month"><?php the_time('M') ?></span>
                        <span class="event-summary__day"><?php the_time('d') ?></span>
                    </a>
                    <div class="event-summary__content">
                        <h5 class="event-summary__title headline headline--tiny "><a
                                    href="<?php the_permalink(); ?>"><?php the_title(); ?> </a></h5>
                        <p>
                            <?php if (has_excerpt()) {
                                echo get_the_excerpt();
                            } else {
                                echo wp_trim_words(get_the_content(), 18);
                            } ?>
                            <a href="<?php the_permalink(); ?>" class="nu gray">Read more</a></p>
                    </div>
                </div>
                <?php
            }
            wp_reset_postdata();
            ?>

            <p class="t-center no-margin"><a href="<?php echo get_post_type_archive_link('coures') ?>"
                                             class="btn btn--blue">View All Courses</a></p>

        </div>
    </div>
    <div class="full-width-split__two">
        <div class="full-width-split__inner">
            <h2 class="headline headline--small-plus t-center">Softwares</h2>
            <?php
            $posts = new WP_Query(array(
                'posts_per_page' => 3,
                'post_type' => 'softwares'
            ));

            while ($posts->have_posts()) {
                $posts->the_post(); ?>
                <div class="event-summary">
                    <a class="event-summary__date event-summary__date--beige t-center" href="<?php the_permalink(); ?>">
                        <span class="event-summary__month"><?php the_time('M') ?></span>
                        <span class="event-summary__day"><?php the_time('d') ?></span>
                    </a>
                    <div class="event-summary__content">
                        <h5 class="event-summary__title headline headline--tiny "><a
                                    href="<?php the_permalink(); ?>"><?php the_title(); ?> </a></h5>
                        <p>
                            <?php if (has_excerpt()) {
                                echo get_the_excerpt();
                            } else {
                                echo wp_trim_words(get_the_content(), 18);
                            } ?>
                            <a href="<?php the_permalink(); ?>" class="nu gray">Read more</a></p>
                    </div>
                </div>
                <?php
            }
            wp_reset_postdata();
            ?>
            <p class="t-center no-margin">
                <a href="<?php echo site_url('index.php/softwares') ?>"
                class="btn btn--yellow">
                    View All Softwares
                    </a>
            </p>
        </div>
    </div>
</div>
<div class="full-width-split group p-bottom-large">
    <div class="full-width-split__one">
        <div class="full-width-split__inner">
            <h2 class="headline headline--small-plus t-center">Course to Download</h2>
            <?php
            $posts = new WP_Query(array(
                'posts_per_page' => 3,
                'post_type' => 'blog',
            ));

            while ($posts->have_posts()) {
                $posts->the_post(); ?>
                <div class="event-summary">
                    <a class="event-summary__date event-summary__date--beige t-center" href="<?php the_permalink(); ?>">
                        <span class="event-summary__month"><?php the_time('M') ?></span>
                        <span class="event-summary__day"><?php the_time('d') ?></span>
                    </a>
                   <div class="event-summary__content">
                <h5 class="event-summary__title headline headline--tiny "><a
                            href="<?php the_permalink(); ?>"><?php the_title(); ?> </a></h5>
                <p>
                    <a href="<?php the_permalink(); ?>">
                        <img class="front-img" src="<?php echo get_field('image_link') ?>" alt="Preview Image">
                    </a>
                    <?php if (has_excerpt()) {
                        echo get_the_excerpt();
                    } else {
                        echo wp_trim_words(get_the_content(), 18);
                    } ?>
                    <a href="<?php the_permalink(); ?>" class="nu gray">Read more</a></p>
            </div>
                </div>
                <?php
            }
            wp_reset_postdata();
            ?>

            <p class="t-center no-margin"><a href="<?php echo get_post_type_archive_link('blog') ?>"
                                             class="btn btn--blue">View All Downloadable</a></p>

        </div>
    </div>
    <div class="full-width-split__two">
        <div class="full-width-split__inner">
            <h2 class="headline headline--small-plus t-center">Games</h2>
            <?php
            $posts = new WP_Query(array(
                'posts_per_page' => 3,
                'post_type' => 'game'
            ));

            while ($posts->have_posts()) {
                $posts->the_post(); ?>
                <div class="event-summary">
                    <a class="event-summary__date event-summary__date--beige t-center" href="<?php the_permalink(); ?>">
                        <span class="event-summary__month"><?php the_time('M') ?></span>
                        <span class="event-summary__day"><?php the_time('d') ?></span>
                    </a>
                   <div class="event-summary__content">
                <h5 class="event-summary__title headline headline--tiny "><a
                            href="<?php the_permalink(); ?>"><?php the_title(); ?> </a></h5>
                <p>
                    <a href="<?php the_permalink(); ?>">
                        <img class="front-img" src="<?php echo get_field('image_link') ?>" alt="Preview Image">
                    </a>
                    <?php if (has_excerpt()) {
                        echo get_the_excerpt();
                    } else {
                        echo wp_trim_words(get_the_content(), 18);
                    } ?>
                    <a href="<?php the_permalink(); ?>" class="nu gray">Read more</a></p>
            </div>
                </div>
                <?php
            }
            wp_reset_postdata();
            ?>
            <p class="t-center no-margin"><a href="<?php echo site_url('index.php/softwares') ?>"
                                             class="btn btn--yellow">View
                    All Games
                </a></p>
        </div>
    </div>
</div>



<?php get_footer()
?>
 

  