<?php

get_header();
pageBanner(array(
    'title' => get_the_archive_title(),
    'subTitle' => get_the_archive_description()
))
?>
    <div class="container container--narrow page-section ">
        <?php
        while (have_posts()) {
            the_post(); ?>
            <div class="post-item">
                <h2 class="headline headline--medium headline--post-title"><a
                            href="<?php the_permalink(); ?>">  <?php the_title(); ?> </a></h2>
                <div class="metabox">
                    <p>Posted By <?php the_author_posts_link() ?> on <?php the_time('dS . F . Y'); ?>
                        in <?php echo get_the_category_list(', ') ?></p>
                </div>
                <div class="generic-content">
                    <?php if(get_field('image_link') ){?>
                         <div class="one-third">
                        <img class="front-img" src="<?php echo get_field('image_link') ?>" alt="">
                    </div><?php
                    } ?>

                    <?php the_excerpt() ?>
                    <p><a class="btn btn--blue" href="<?php echo the_permalink(); ?>">Continue reading </a></p>
                </div>
            </div>
            <?php
        }
        echo paginate_links();
        ?>
    </div>
<?php


get_footer();
?>