<?php get_header() ?>

<?php

while (have_posts()) {
    the_post();
    pageBanner(array(
        'title' => get_the_title(),
        'photo' => 'https://images.unsplash.com/photo-1540292446300-4f97945502f3?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=5628a713a4899bb205b7f07d05d88c54&auto=format&fit=crop&w=1050&q=80'
    ));
    ?>

    <div class="container container--narrow page-section">
        <?php
        $theParent = wp_get_post_parent_id(get_the_ID());
        if ($theParent) { ?>
            <div class="metabox metabox--position-up metabox--with-home-link">
                <p><a class="metabox__blog-home-link" href="<?php echo get_permalink($theParent) ?>"><i
                                class="fa fa-home" aria-hidden="true"></i> Back
                        to <?php echo get_the_title($theParent) ?></a> <span
                            class="metabox__main"><?php the_title() ?></span></p>
            </div>
            <?php
        }
        ?>


        <div class="generic-content">
            <?php
            get_search_form();
            ?>
        </div>

    </div>
    <?php
}
?>
<?php get_footer() ?>