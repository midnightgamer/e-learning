<?php get_header() ?>

<?php

while (have_posts()) {
    the_post();
     $categories = get_the_category();
    $id =  $categories[0]->cat_name;
    pageBanner(array(
        'cat' =>  $id
        )
    );

 ?>

    <div class="container container--narrow page-section">
        <div class="metabox metabox--position-up metabox--with-home-link">
            <p><a class="metabox__blog-home-link" href="<?php echo site_url('index.php/softwares'); ?>">
                    <i class="fa fa-home" aria-hidden="true"></i>
                    All Software
                </a>
                <span class="metabox__main">
                    Posted By <?php the_author_posts_link() ?> in
                    <?php echo get_the_category_list(', ')
                    ?>
                </span>
            </p>
        </div>

        <div class="single__blog generic-content ">
            <?php
            $likeCount = new WP_Query(array(
                'post_type' => 'like',
                'meta_query' => array(
                    array(
                        'key' => 'like_id',
                        'compare' => '=',
                        'value' => get_the_ID()
                    )
                )
            ));


            if (is_user_logged_in()){

                $existQuery = new WP_Query(array(
                    'author' => get_current_user_id(),
                    'post_type' => 'like',
                    'meta_query' => array(
                        array(
                            'key' => 'like_id',
                            'compare' => '=',
                            'value' => get_the_ID()
                        )
                    )
                ));
                if($existQuery->found_posts){
                    $existStatus = 'yes';
                }
            }
            ?>
            <span class="like-box" data-like="<?php echo $existQuery->posts[0]->ID; ?>"  data-id="<?php the_ID() ?>"  data-exists="<?php echo $existStatus?>">
               <i class="fa fa-heart-o"></i>
               <i class="fa fa-heart"></i>
               <span class="like-count"><?php echo $likeCount->found_posts; ?></span>
           </span>
            <?php get_field('page_banner_background')['url'];
            ?>
            <div class="front-img--single ">
                <img src="<?php echo get_field('software_img_link') ?>" alt="">
            </div>
            <div class="content">
                 <?php
            the_content();
            ?>
            </div>

            <hr class="section-break">
            <button class="btn btn--orange2"><a href="<?php echo get_field('download_link'); ?>">Download Now</a>
            </button>
        </div>
    </div>

    <?php
}
?>
<?php get_footer() ?>