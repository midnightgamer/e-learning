<?php
add_action('rest_api_init', 'registerSearch');
function registerSearch(){
    register_rest_route('e-learning/v1','search',array(
        'methods'=> WP_REST_SERVER::READABLE,
        'callback'=>'searchResult'

    ));
}
function searchResult($props){
    $mainQuery = new WP_Query(array(
        'post_type' =>array(
            'blog','page','coures','softwares','game'
        ),
        's' => sanitize_text_field($props['term']),
    ));

    $result = array(
        'generalInfo' =>array(

        ),
//        'professors' => array(
//
//        ),
//        'events' =>array(
//
//        ),
//        'campuses' =>array(
//
//        ),
        'course'=> array(

        )
        ,
        'softwares'=> array(

        ),
        'games'=>array(

        )
    );
    while ($mainQuery->have_posts()){
        $mainQuery -> the_post();
        if (get_post_type() == 'blog' OR get_post_type() == 'page'){

            array_push($result['generalInfo'] , array(
                'title' => get_the_title(),
                'url' => get_the_permalink(),
                'post_type' => get_post_type(),
                'authorName' => get_the_author(),
            ));
        }
        if (get_post_type() == 'softwares'){

            array_push($result['softwares'] , array(
                'title' => get_the_title(),
                'url' => get_the_permalink(),
            ));
        }
        if (get_post_type() == 'game'){

            array_push($result['games'] , array(
                'title' => get_the_title(),
                'url' => get_the_permalink(),
            ));
        }
//        if (get_post_type() == 'campus'){
//
//            array_push($result['campuses'] , array(
//                'title' => get_the_title(),
//                'url' => get_the_permalink(),
//            ));
//        }
//        if (get_post_type() == 'event'){
//
//            array_push($result['events'] , array(
//                'title' => get_the_title(),
//                'url' => get_the_permalink(),
//
//            ));
//        }
        if (get_post_type() == 'coures'){
            array_push($result['course'] , array(
                 'title' => get_the_title(),
                'url' => get_the_permalink(),
                'post_type' => get_post_type(),
                'authorName' => get_the_author(),
            ));
        }

    }
    return $result;
}
