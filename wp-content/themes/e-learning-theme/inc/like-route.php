<?php

add_action('rest_api_init', 'likeRoutes');

function likeRoutes()
{
    register_rest_route('elearning/v1', 'manageLike', array(
        'methods' => 'POST',
        'callback' => 'createLike'
    ));

    register_rest_route('elearning/v1', 'manageLike', array(
        'methods' => 'DELETE',
        'callback' => 'deleteLike'
    ));
}

function createLike($data)
{
    if (is_user_logged_in()) {
        $mainId = sanitize_text_field($data['id']);

            $existQuery = new WP_Query(array(
                    'author' => get_current_user_id(),
                    'post_type' => 'like',
                    'meta_query' => array(
                        array(
                            'key' => 'like_id',
                            'compare' => '=',
                            'value' => $mainId
                        )
                    )
                ));

        if ($existQuery->found_posts == 0 AND get_post_type($mainId)){
                return wp_insert_post(array(
            'post_type' => 'like',
            'post_status' => 'publish',
            'post_title' => 'Wow',
            'meta_input' => array(
                'like_id' => $mainId
            )
        ));

    } else {
        die("Login First");
    }
        }
        else{
            die("Cannot like twice");
        }


}

function deleteLike($data)
{
   $likeId = sanitize_text_field($data['like']);
   if (get_current_user_id() == get_post_field('post_author',$likeId) AND get_post_type($likeId) == 'like'){
          wp_delete_post($likeId , true);
          return "Done";
   }
   else{
       die("Can delete");
   }
}