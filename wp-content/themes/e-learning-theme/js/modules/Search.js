import $ from 'jquery';

export default class Search {
    //Section Where create our Object
    constructor() {
        this.addSearchHtml();
        this.openButton = $(".js-search-trigger");
        this.closeButton = $(".search-overlay__close");
        this.searchOverlay = $(".search-overlay");
        this.resultsDiv = $("#search-overlay__results");
        this.searchField = $("#search-term");
        this.events();
        this.isOverlayOpen = false;
        this.isspinnerShown = false;
        this.previousValue = "";
        this.typingTimer = "";

    }

    //2. Events Here
    events() {
        this.openButton.on("click", this.openOverlay.bind(this));
        this.closeButton.on("click", this.closeOverlay.bind(this));
        $(document).on("keydown", this.keyPressDispatcher.bind(this));
        this.searchField.on("keyup", this.typingLogic.bind(this))
    }

    //3.Methods here
    typingLogic() {
        if (this.searchField.val() != this.previousValue) {
            clearTimeout(this.typingTimer);
            if (this.searchField.val()) {
                if (!this.isspinnerShown) {
                    this.resultsDiv.html('<div class="spinner-loader"></div> ');
                    this.isspinnerShown = true;
                }
                this.typingTimer = setTimeout(this.getResult.bind(this), 550);
            }
            else {
                this.resultsDiv.html("");
                this.isspinnerShown = false;
            }
        }
        this.previousValue = this.searchField.val();
    }

    getResult() {
        $.getJSON(rootUrl.root_url + '/index.php/wp-json/e-learning/v1/search?term=' + this.searchField.val(), (results) => {
            this.resultsDiv.html(`
             <div class="row">
             <div class="one-third">
             <h2 class="search-overlay__section-title">General Info</h2>
		       ${results.generalInfo.length ? '<ul class="link-list min-list">' : '<p>No general information matches that search.</p>'}
              ${results.generalInfo.map(item => `<li><a href="${item.url}">${item.title}</a> ${item.post_type == 'post' ?`by ${item.authorName}` : ''}</li>`).join('')}
            ${results.generalInfo.length ? '</ul>' : ''}
        </div>
                     <div class="one-third">
                     <h2 class="search-overlay__section-title">Available Courses</h2>
					       ${results.course.length ? '<ul class="link-list min-list">' : '<p>No  Enrollable course matches that search. <a href="index.php/coures">View All Course</a> </p>'}
              ${results.course.map(item => `<li><a href="${item.url}">${item.title}</a> </li>`).join('')}
            ${results.course.length ? '</ul>' : ''}
        </div> 
        
        <div class="one-third">
                     <h2 class="search-overlay__section-title">Softares</h2>
					       ${results.softwares.length ? '<ul class="link-list min-list">' : '<p>No  software matches that search. <a href="index.php/softwares">View All software</a> </p>'}
              ${results.softwares.map(item => `<li><a href="${item.url}">${item.title}</a> </li>`).join('')}
            ${results.softwares.length ? '</ul>' : ''}
        </div>
         <div class="one-half">
                     <h2 class="search-overlay__section-title">Games</h2>
					       ${results.games.length ? '<ul class="link-list min-list">' : '<p>No  games matches that search. <a href="index.php/games">View All Games</a> </p>'}
              ${results.games.map(item => `<li><a href="${item.url}">${item.title}</a> </li>`).join('')}
            ${results.games.length ? '</ul>' : ''}
        </div>
                  
                     
        </div>
`);
            this.isspinnerShown = false;
        });



    }

    openOverlay() {
        this.searchOverlay.addClass("search-overlay--active");
        $("body").addClass("body-no-scroll");
        this.searchField.val("");
        setTimeout(() => this.searchField.focus(), 301);
        this.isOverlayOpen = true;
        return false;
    }

    closeOverlay() {
        this.searchOverlay.removeClass("search-overlay--active");
        $("body").removeClass("body-no-scroll");
        this.isOverlayOpen = false;


    }

    keyPressDispatcher(key) {
        if (key.keyCode == 83 && !this.isOverlayOpen && !$("input , textarea").is(':focus')) {
            this.openOverlay();
        }
        if (key.keyCode == 27 && this.isOverlayOpen) {
            this.closeOverlay();
        }
    }

    addSearchHtml() {
        $("body").append(`
        <div class="search-overlay ">
    <div class="search-overlay__top">
        <div class="container">
            <i class="fa fa-search search-overlay__icon" aria-hidden="true"></i>
            <input type="text" class="search-term" placeholder="What you looking for ?" id="search-term">
            <i class="fa fa-window-close search-overlay__close" aria-hidden="true"></i>
        </div>
    </div>
    <div class="container">
        <div id="search-overlay__results"></div>
    </div>
</div>

        `)
    }
}
