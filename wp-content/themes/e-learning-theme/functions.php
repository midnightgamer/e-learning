<?php
require get_theme_file_path('/inc/search-route.php');
require get_theme_file_path('/inc/like-route.php');
function custom_rest()
{
    register_rest_field('post', 'authorName', array(
        'get_callback' => function () {
            return get_the_author();
        }
    ));
}

add_action('rest_api_init', 'custom_rest');


function resourse()
{
    wp_enqueue_script('googleMap', '//maps.googleapis.com/maps/api/js?key=AIzaSyCj1ho8sTPwPKVh3uqP-9jI_DpMttPOFF0', NULL, 1.0, true);
    wp_enqueue_script('silder', get_theme_file_uri('/js/scripts-bundled.js'), NULL, microtime(), true);
    wp_enqueue_style('coustom-fonts', '//fonts.googleapis.com/css?family=Lato:300,400,700,700i');
    wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
    wp_enqueue_style('main_style', get_stylesheet_uri(), NULL, microtime());
    wp_localize_script('silder', 'rootUrl', array(
        'root_url' => get_site_url(),
        'nonce' => wp_create_nonce('wp_rest')
    ));
}

add_action('wp_enqueue_scripts', 'resourse');

function load_feature()
{
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');
    add_image_size('Landscape', 400, 260, true);
    add_image_size('Portrait', 480, 650, true);
    add_image_size('pageBanner', 1500, 350, true);
//    register_nav_menu('headerMenuLocation', 'Header Menu Location');
//    register_nav_menu('footerLocationOne', 'Footer Location One');
//    register_nav_menu('footerLocationTwo', 'Footer Location Two');

}


add_action('after_setup_theme', 'load_feature');

//function adjust_quries($query)
//{
//    if (!is_admin()) {
//        $query->set('posts_per_page', '10');
//    }
//}
//
function adjust_Myquries($query)
{
    if (!is_admin() AND is_post_type_archive('campus') And $query->is_main_query()) {
        $query->set('posts_per_page', -1);
    }
}

add_action('pre_get_posts', 'adjust_Myquries');
function ourMap($api)
{
    $api['key'] = 'AIzaSyCj1ho8sTPwPKVh3uqP-9jI_DpMttPOFF0';
    return $api;
}

add_filter('acf/fields/google_map/api', 'ourMap');

function pageBanner($prop = NULL)
{

    if (!$prop['title']) {
        $prop['title'] = get_the_title();
    }
    if (!$prop['subTitle']) {
        $prop['subTitle'] = get_field('page_banner_subtitle');
    }

    if (!$prop['photo']) {
        if (get_field('page_banner_background')) {
            $banner = get_field('page_banner_background');
            $banner = $banner['url'];
            $prop['photo'] = $banner;
        }
        else {
            $prop['photo'] = get_theme_file_uri('images/ocean.jpg');
        }
    }

    if ($prop['cat']) {
        if ($prop['cat'] == 'IDE') {
            $prop['photo'] = get_theme_file_uri('images/ides.jpg');
        }
        if ($prop['cat'] == 'Games') {
            $prop['photo'] = get_theme_file_uri('images/gamer.jpeg');
        }
        if ($prop['cat'] == 'Audio Tool') {
            $prop['photo'] = get_theme_file_uri('images/audio.jpg');
        }
        if ($prop['cat'] == 'Graphics Tool') {
            $prop['photo'] = get_theme_file_uri('images/graphics.jpg');
        }
    }



    ?>
    <div class="page-banner">
        <div class="page-banner__bg-image"
             style="background-image: url(<?php echo $prop['photo'] ?>);"></div>
        <div class="page-banner__content container container--narrow">
            <h1 class="page-banner__title"><?php echo $prop['title'] ?></h1>
            <div class="page-banner__intro">
                <p><?php echo $prop['subTitle']; ?></p>
            </div>
        </div>
    </div>

    <?php
}

add_action('admin_init', 'redirectSubs');
function redirectSubs()
{
    $user = wp_get_current_user();
    if (count($user->roles) == 1 AND $user->roles[0] == 'subscriber') {
        wp_redirect(site_url('/'));
        exit;
    }

}

add_action('wp_loaded', 'noSubsAdminBar');
function noSubsAdminBar()
{
    $user = wp_get_current_user();
    if (count($user->roles) == 1 AND $user->roles[0] == 'subscriber') {
        show_admin_bar(false);

    }

}

//Login Screen
add_filter('login_headerurl', 'ourUrl');
function ourUrl()
{
    return esc_url((site_url('/')));
}

add_action('login_enqueue_scripts', 'loginCSS');
function loginCSS()
{
    wp_enqueue_style('main_style', get_stylesheet_uri(), NULL, microtime());
    wp_enqueue_style('coustom-fonts', '//fonts.googleapis.com/css?family=Lato:300,400,700,700i');

}

  function post_types_author_archives($query) {
        if ($query->is_author)
                // Add 'books' CPT and the default 'posts' to display in author's archive
                $query->set( 'post_type', array('coures', 'post','softwares','game') );
        remove_action( 'pre_get_posts', 'custom_post_author_archive' );
    }
    add_action('pre_get_posts', 'post_types_author_archives');

function my_query_post_type($query) {
    if ( is_category() && ( ! isset( $query->query_vars['suppress_filters'] ) || false == $query->query_vars['suppress_filters'] ) ) {
        $query->set( 'post_type', array( 'post', 'softwares','coures','game' ) );
        return $query;
    }
}
add_filter('pre_get_posts', 'my_query_post_type');