<?php

get_header();
pageBanner(array(
    'title' => "Search Results",
    'subTitle' => "You searched for &ldquo;". get_search_query() ."&rdquo;"
))
?>
    <div class="container container--narrow page-section">
        <?php
        if(have_posts()){

            while (have_posts()) {
                the_post(); ?>
                <div class="post-item">
                    <h2 class="headline headline--medium headline--post-title"><a
                            href="<?php the_permalink(); ?>">  <?php the_title(); ?> </a></h2>
                    <div class="one-third">
                        <img src="<?php echo get_field('image_link') ?>" alt="">
                    </div>
                    <div class="metabox">
                        <p>Posted By <?php the_author_posts_link() ?> on <?php the_time('dS . F . Y'); ?>
                            in <?php echo get_the_category_list(', ') ?></p>
                    </div>
                    <div class="generic-content">
                        <p><a class="btn btn--blue" href="<?php echo the_permalink(); ?>">Continue reading </a></p>
                    </div>
                </div>


                <?php
            }
            echo paginate_links();
        }
        else{
            echo '<h2 class="headline headline--small-plus">No result match that search</h2>';
            get_search_form();
        }
        ?>
    </div>
<?php


get_footer();
?>