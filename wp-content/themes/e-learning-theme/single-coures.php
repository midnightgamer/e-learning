<?php get_header() ?>

<?php

while (have_posts()) {
    the_post();
    pageBanner();
    ?>

    <div class="container container--narrow page-section">
        <div class="metabox metabox--position-up metabox--with-home-link">
            <p><a class="metabox__blog-home-link" href="<?php echo site_url('index.php/coures'); ?>">
                    <i class="fa fa-home" aria-hidden="true"></i>
                    All Enrollable
                </a>
                <span class="metabox__main">
                    Posted By <?php the_author_posts_link() ?>
                </span>
            </p>
        </div>
        <div class="single__blog generic-content">
                <?php
            $likeCount = new WP_Query(array(
                'post_type' => 'like',
                'meta_query' => array(
                    array(
                        'key' => 'like_id',
                        'compare' => '=',
                        'value' => get_the_ID()
                    )
                )
            ));


            if (is_user_logged_in()){

                $existQuery = new WP_Query(array(
                    'author' => get_current_user_id(),
                    'post_type' => 'like',
                    'meta_query' => array(
                        array(
                            'key' => 'like_id',
                            'compare' => '=',
                            'value' => get_the_ID()
                        )
                    )
                ));
                if($existQuery->found_posts){
                    $existStatus = 'yes';
                }
            }
            ?>
            <span class="like-box" data-like="<?php echo $existQuery->posts[0]->ID; ?>"  data-id="<?php the_ID() ?>"  data-exists="<?php echo $existStatus?>">
               <i class="fa fa-heart-o"></i>
               <i class="fa fa-heart"></i>
               <span class="like-count"><?php echo $likeCount->found_posts; ?></span>
           </span>
            <?php get_field('page_banner_background')['url'];
            ?>
            <div class="single--image ">
                <img src="<?php echo get_field('image_link') ?>" alt="">
            </div>
            <?php
            the_content();
            ?>
            <hr class="section-break">
            <button class="btn btn--orange2"><a href="<?php echo get_field('coures_link'); ?>" target="_blank">Enroll
                    Now</a>
            </button>
        </div>
    </div>

    <?php
}
?>
<?php get_footer() ?>