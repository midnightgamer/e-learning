<?php get_header() ?>

<?php

while (have_posts()) {
    the_post();
    pageBanner() ?>
    <div class="container container--narrow page-section">
        <div class="metabox metabox--position-up metabox--with-home-link">
            <p><a class="metabox__blog-home-link" href="<?php echo get_post_type_archive_link('event'); ?>"><i
                            class="fa fa-home" aria-hidden="true"></i> Event Home </a> <span
                        class="metabox__main"> <?php the_title(); ?> </span></p>
        </div>
<!--        <div class="genric-content">-->
<!--            --><?php //the_content(); ?>
<!--            --><?php
//            $relatedProgram = get_field('related_programs');
//            if ($relatedProgram) {
//                echo '<hr class="section-break">';
//                echo '<h2 class="headline headline--medium">Post(s)</h2>';
//                echo '<ul class="link-list min-list">';
//            }
//            foreach ($relatedProgram as $program) {
//                ?>
<!--                <li class="professor-card__list-item">-->
<!--                    <a class="professor-card" href="--><?php //echo get_the_permalink(); ?><!--">-->
<!--                        <img src="" class="professor-card__image">-->
<!--                        <span class="professor-card__name">--><?php //echo the_title(); ?><!--</span>-->
<!--                    </a>-->
<!--                </li>-->
<!--                --><?php
//            }
//            echo '</ul>';
//
//            ?>
<!--        </div>-->
    </div>

    <?php
}
?>
<?php get_footer() ?>