===Admin Custom Login===
Contributors: Zia Imtiaz
Tags: admin login, custom login, login customizer, login page styler ,wp login security
Donate link: http://web-settler.com/login-page-styler/
Requires at least: 4.0
Tested up to: 4.9.8
Stable tag: 5.3.5
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Customize Your WordPress Login Screen Amazingly � Add Own Logo, Login Form Positions, Background Image and so much more. WordPress Custom Login  plugin allows you to easily customize the layout of login, admin login, client login, register  pages

== Description ==
Admin Custom Login Plugin - Login page styler give\'s your admin login , login , registration and password recovery pages new customized look  in few minutes zero coding skill required.More Features then any other custom login plugin , Full control over login page styling  and very easy to use .
  
Create unique  login design  or admin login design  with custom login plugin, Almost every element on login  page is customize-able with custom login page styler plugin.Design beautiful and eye catching  login page styles in few Minutes .

 Custom Login Plugin - Login Page Styler  is  well tested,
if you find a bug please open a ticket in the support request.
Every issue will be fixed asap!

If you forgotten and need it: your standard WordPress login is \"your_domain/wp-admin/\" or \"your_domain/wp-login.php\". 


 Custom Login Plugin\'s General Settings
You can hide/show custom login logo , you can hide/show login error messages , you can hide/show navigation links: lost paswword /register.

 Custom Login Plugin\'s  Logo Settings
This settings area of Custom Login - Login Page Styler plugin  helps you add your own logo instead of WordPress logo on login page . You can also change the width and height of the login logo from 80 px X 80 px  to  custom width and height.
If you don\'t want to show your own logo or WordPress logo on login page you  have the ability to do so by just hiding the login logo.You can also add  custom link to logo .   

 Custom Login Plugin Login Page Background Settings
This settings area  of Ultimate Login plugin gives you ability to add a  background image to the grey background of login page  giving users a  attractive look .You can  also change the  background color of the login page if you don\'t want to add a background image  

 Custom Login Plugin Login Form Settings
This settings area of Ultimate login plugin  give\'s you full control to customize the details of login form you can change form color , add login form background image , change form border (size,color,style),  change input field border(size,color,style) , login form label color and size and login form color with opacity , login form position  can also be change  to left right top bottom. 

 Custom Login Plugin Google Fonts
This settings area of Ultimate login plugin helps add google fonts for login label , login error message , input field , remember me , login button and navigation links. Chose your favorite font from hundreds of google fonts and match your login page with your site\'s theme.

 Custom Login Plugin Login Button Settings
This settings area of Ultimate login plugin helps changing login button color and login button hover color and can also change the border radius border color.

 Custom Login Plugin Navigation Links
This settings area of Ultimate login plugin helps change the navigation links color and size , you can chose color from wordpress color picker .

 Custom Login Plugin Google reCaptcha
Google reCAPTCHA is built for security. Armed with state of the art technology, it always stays at the forefront of spam and abuse fighting trends. reCAPTCHA is on guard for you, so you can rest easy
To increase security of your login page you can add google reCaptcha to your login form . It helps fight against bots and spammers to login to your site . To use Google ReCaptcha  on registration and password recovery page you have to upgrade to pro version of the plugin.

 Custom Login Plugin Limit Login Attempts
Brute force attack may be used by criminals to crack encrypted data , or by security analysts to test an organization\'s network security. If you adopt the use of this plugin, it will limit the number of times a user can attempt to log into your account.
Limit Login Attempts for login protection, protect site from brute force attacks.Brute Force Attack aims at being the simplest kind of method to gain access to a site: it tries usernames and passwords, over and over again, until it gets in. Limit Login Attempt & Login Page Styler Login plugin limit rate of login attempts and block IP temporarily.



SIMPLE AND EASY WORKFLOW .
Say goodbye to boring, simple, plain WordPress login page and create one with you our own logo, your own branding, colors, background and so much more . Login Page Styler  is different by design because it is focused on efficiency, usability and reliability. It is created in a way that molds simplicity and functionality into a powerful yet usable login customizer. Login Page Styler make you stand out with half the effort.
Brand your login page with your branding , More features then any other custom login plugin. 


 Custom Login Plugin Powerful And Easy Setting Panel:
Custom login - login page styler\'s Setting Panel is very simple and user friendly. There you are able to design your login page without any hesitation.


 Custom Login Plugin Responsive Design:
Custom login - login page styler  design is totally mobile compatible.


 Custom Login Plugins Unlimited Color Scheme:
Custom login - login page styler  gives you ability update plugin design with unlimited colors. Color option is available for login form , login background , login form input field , login button , border etc.

 Custom Login Plugins CSS Support :
Plugin fully supports custom login CSS ability for styling. If you want to make it look perfect, add your custom codes and change the styles of almost every detail.

 Custom Login Plugin  Style and Design Options : 
Professional version of the plugin provides more than 45 options for design available in the Design section of the plugin, which will help to match the style of your website making it look better and be more professional to its visitors.

 Custom Login Google reCaptcha : 
The security keys from Google which will protects the website from harming programs and robots. The feature to add a reCaptcha and is well known by many site owners.

For additional information about sending or receiving of data about custom login - login page styler , please, read the questions and answers on FAQ page. Premium version 
Supports full features.

 Custom Login plugin is also compatible with any plugin that hooks in the login form, including

1. BuddyPress,
1. bbPress,
1. Limit Login Attempts,
1. Captcha plugins. 

Features :-
 1. Plugin on/off.
 1. Add your own custom logo on login or admin login page with custom login.
 1. Change logo title with custom login.
 1. Hide login logo with custom login .
 1. Hide login error message with custom login . 
 1. Hide login lost password link with custom login.
 1. Hide login back to blog link with custom login .
 1. Custom login logo width.
 1. Custom login logo height.
 1. Custom login logo link .
 1. Custom login form position.
 1. Change login form label font size.
 1. Change login form label color.
 1. Change login form label font style.
 1. Custom Login Logo upload button.
 1. Custom Login body background image upload button.
 1. Custom Login form background color.
 1. Custom login label fonts.
 1. Customize login form border width.
 1. Add custom login form border color , border style.
 1. Add custom login form textbox border size color style radius.
 1. Change login form button color , border size, border color.
 1. Change login form button border radius , border style.
 1. Change button hover  border size  border color, border style.
 1. Add navigation links color , navigation links hover color , links size with custom login.
 1. And so many more coming soon.

== Installation ==
Install custom login - login page styler plugin via wordpress dashboard :
	
1. Go to the Plugins Menu in WordPress.
1. Search for plugin \\\"Custom Login - Login Page Styler\\\".
1. Click \\\"Install\\\".
1. After Installation click activate to start using the Custom Login - Login Page Styler on your website.

* Go to Custom Login - Login Page Styler  from Dashboard menu.
* Enable Custom Login - Login Page Styler feature and  do all the styling you want to do on your login .
* Hit save settings and  visit your login page to see the magic .
 
Install  custom login - login page styler plugin via FTP
    
1.  Download the Custom Login - Login Page Styler plugin
1.  Unzip Custom Login - Login Page Styler plugin
1.  Copy the Custom Login - Login Page Styler folder 
1.  Open the ftp \\\\\\\\wp-content\\\\\\\\plugins\\\\\\\\
1.  Paste the folder inside plug-ins folder 
1.  Go to admin panel => open item \\\\\\\"Plugins\\\\\\\" => activate Custom Login - Login Page Styler 

== Frequently Asked Questions ==
= Is it legal to change WordPress Logo and texts? =

Yes. WordPress gives you the permission to change whole login screen to yours by using WordPress hooks used in custom login - login page styler.

= Custom login - login page styler installed and activated  but not working, what should i do?=

Open custom login -  login page styler settings page from dashboard and click yes on the first option Enable plugin, then save settings.

=  What image size to use for login background image? =

Whatever image size you want you can use for login page  , it will set as cover because custom login login page styler is designed to set the background image cover

=  What image size to use for logo image for login page? =

In free version you can use 80 X 80 px  and  in premium version  you can use 300 X 200 px logo for login page  with custom login - login page styler plugin  .   

=  What if I need to style login page element if plugin doesn\'t offer? =

Custom login - login page styler give you option to customize nearly every element on login page, but if there\\\'s is an element you think needs customization, you can do that in \\\"Custom CSS\\\" box available in custom login - login page styler plugin .

=  I\'m not a designer for custom login, can you  customize our login ? =

Yes, we can customize your login page for you which will match your WordPress site\\\'s theme. for that Contact us at ziaimtiaz21@gmail.com.

=  Do I need coding skills to design my login page? =

No, Custom login - Login Page Styler is created for those who don\\\'t know how to code. You can just choose from given options how should a specific element will looks like. 

=  Can i change login form position ? =

Yes, Custom login - login page styler  gives you ability to change login form position.


= . Can i change login form opacity to transparent ? =

Yes, In settings page of  custom login - login page styler we have created a RGBA input field  in which you just have to paste a color value with its alpha value to make  form transparent for example 255,255,255,0.5  the last vale(0.5) is the opacity value to make login form transparent. 

=  Can i change login form label color with this plugin ? =

Yes, You can change  login form label  color with custom login - login page styler.

=  Can i change login form label font style ? =

Yes, You can change  login form label font with custom login - login page styler.


== Screenshots ==
1. custom login page screenshot  . 

2. custom login page screenshot  .   

3. custom login page screenshot  .

4. custom login page screenshot  .

5. custom login page screenshot  .

6. custom login page screenshot  .

7. custom login page screenshot  .

8. custom login page screenshot  .

9. Custom lost-password page screenshot  .

10. Custom login page screenshot  .

11. Custom login page screenshot  .

12. Custom login page screenshot  .

13. Custom login page screenshot  .

14. Custom login page screenshot  .

15. Custom login page screenshot  .

16. Custom login page screenshot  .

== Changelog ==
Version 4.2.5 Released
  * Fixed Logo Width height issue.
  * Fixed Cover Issue.
  * Limit Login Attempts

Version 3.1.5 Released

Added New Features

  * Custom Login Templates.
  * Google ReCaptcha for login 
  * Slide value selector for text size ,border size ,border style.
  * Navigation link hover color.
  * Navigation link text size.
  * Google font  for login form.

Version 2.1 released.Added New Features
	
  * Logo upload button.
  * Login Body background image upload button.
  * Login Form background image upload button.
  * Custom Login Label fonts.
  * Custom Login border size.
  * Custom Login Form border color.
  * Login Form border style.
  * Login Form input field border size.
  * Login Form input field border color.
  * Custom Login Form input field border color.
  * Custom Login Form input field border style.